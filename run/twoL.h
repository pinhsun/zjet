#include <iostream>
#include <fstream>

#include "puweicalc.h"
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TVector2.h>
#include <TF1.h>
#include <TMath.h>
#include <string>
#include "untuplizer.h"
#include "tree.h"
#include "Select.h"
#include "getSF.h"
#include <vector>

//using namespace std;

TH2* h2JvZPtE = new TH2F("h2JvZPtE","2d Jets v Z Pt",100,0.0,100.0,100,0.0,100.0);
TH2* h2JvZPtM = new TH2F("h2JvZPtM","2d Jets v Z Pt",100,0.0,100.0,100,0.0,100.0);

PUWeightCalculator puCalcGJ_69nb;
PUWeightCalculator puCalcGJ_69p2nb;
PUWeightCalculator puCalcGJ_65nb;
PUWeightCalculator puCalcGJ_63nb;

void initPUWCal(){
	puCalcGJ_69nb.Init("external/PU_histo_13TeV_2016_GoldenJSON_72400nb.root");
    puCalcGJ_69p2nb.Init("external/PU_histo_13TeV_2016_GoldenJSON_69200nb.root");
    puCalcGJ_65nb.Init("external/PU_histo_13TeV_2016_GoldenJSON_66000nb.root");
    puCalcGJ_63nb.Init("external/PU_histo_13TeV_2016_GoldenJSON_63000nb.root");

}

void eeToZ(string path,TH1* hE,TH1* hE2,TH1* hJetE,TH1* ZvBPt){

	TreeReader data1(TString(path), "ggNtuplizer/EventTree");
	TLorentzVector ele1, ele2, Z, jet1, jet2, jets;
	Long64_t ev1 = 0;
	Long64_t ev2 = -1;
	if (ev2 < 0) ev2 = data1.GetEntriesFast();
	if (ev2 > data1.GetEntriesFast()) ev2 = data1.GetEntriesFast();
	for (Long64_t ev = ev1; ev < ev2; ++ev) {
		    data1.GetEntry(ev);
			readggtree(data1);
			vector<int> eleIndex;
			vector<int> selectedEle;
			if(!(hlt>>40&1)&&!(hlt>>5&1)) continue;
			passEleId(selectedEle);

			if(selectedEle.size()>1&&elePt[0]>25.){
				
				ele1.SetPtEtaPhiM(elePt[selectedEle[0]], eleEta[selectedEle[0]], elePhi[selectedEle[0]], 0.000511);
				ele2.SetPtEtaPhiM(elePt[selectedEle[1]], eleEta[selectedEle[1]], elePhi[selectedEle[1]], 0.000511);
				Z=ele1+ele2;
				if(Z.M()<50.) continue;
				hE->Fill(Z.M());
				vector<int> selectedJet;
				passJetId(selectedJet);
				if(selectedJet.size()>1){
					
					jet1.SetPtEtaPhiE(jetPt[selectedJet[0]],jetEta[selectedJet[0]],jetPhi[selectedJet[0]],jetEn[selectedJet[0]]);
					jet2.SetPtEtaPhiE(jetPt[selectedJet[1]],jetEta[selectedJet[1]],jetPhi[selectedJet[1]],jetEn[selectedJet[1]]);
					jets=jet1+jet2;

					hE2->Fill(Z.M());
					hJetE->Fill(jet1.M());
					hJetE->Fill(jet2.M());
					ZvBPt->Fill((jets.Pt()/Z.Pt()));
					h2JvZPtE->Fill(jets.Pt(),Z.Pt());
				}
		}
	}
}
void mumuToZ(string path,TH1* hM,TH1* hM2,TH1* hJetM,TH1* ZvBPt){
	TreeReader data2(TString(path), "ggNtuplizer/EventTree");
	Long64_t ev2=data2.GetEntriesFast();
	TLorentzVector Mu1, Mu2, Z, jet1, jet2, jets;
	for (Long64_t ev = 0; ev < ev2; ev++) {
		    data2.GetEntry(ev);
			readggtree(data2);
			if(!(hlt>>14&1)) continue;
			vector<int> eleIndex;
			vector<int> selectedMu;
			passMuonId(selectedMu);

			if(selectedMu.size()>1&&muPt[0]>20.){
				Mu1.SetPtEtaPhiM(muPt[selectedMu[0]], muEta[selectedMu[0]], muPhi[selectedMu[0]], 0.104);
				Mu2.SetPtEtaPhiM(muPt[selectedMu[1]], muEta[selectedMu[1]], muPhi[selectedMu[1]], 0.104);
				Z=Mu1+Mu2;
				if(Z.M()<50.) continue;
				hM->Fill(Z.M());
				vector<int> selectedJet;
				passJetId(selectedJet);
				if(selectedJet.size()>1){
					
					jet1.SetPtEtaPhiE(jetPt[selectedJet[0]],jetEta[selectedJet[0]],jetPhi[selectedJet[0]],jetEn[selectedJet[0]]);
					jet2.SetPtEtaPhiE(jetPt[selectedJet[1]],jetEta[selectedJet[1]],jetPhi[selectedJet[1]],jetEn[selectedJet[1]]);
					jets=jet1+jet2;

					hM2->Fill(Z.M());
					hJetM->Fill(jet1.M());
					hJetM->Fill(jet2.M());
					ZvBPt->Fill((jets.Pt()/Z.Pt()));
					h2JvZPtM->Fill(jets.Pt(),Z.Pt());
				}
		}
	}
}
void genWe(TreeReader& data1){
	if(isData){
		genWeight_=0;
		PUWeight_69p2nb = 0.;
	}else{
		float* puTrue = data1.GetPtrFloat("puTrue");
		PUWeight_69p2nb = (float) puCalcGJ_69p2nb.GetWeight(run, puTrue[1]); // in-time PU
		if(genWeight>0){
			genWeight_=1;
		}else {
			genWeight_=-1;
		}
	}	
}
void eeToZ(string path,TTree* OT,bool isMCReco=false){

	TreeReader data1(TString(path), "ggNtuplizer/EventTree");
	TLorentzVector ele1, ele2, Z, jet1, jet2, jets;
	int neg,pot;//negative postive
	Long64_t ev1 = 0;
	Long64_t ev2 = -1;
	float ZM=0;
	if (ev2 < 0) ev2 = data1.GetEntriesFast();
	if (ev2 > data1.GetEntriesFast()) ev2 = data1.GetEntriesFast();
	for (Long64_t ev = ev1; ev < ev2; ++ev) {
		    data1.GetEntry(ev);
			readggtree(data1);

    		if ( (hlt>>40&1) || (hlt>>5&1) ) trig_Ele23_Ele12 = 1;
    		else trig_Ele23_Ele12 = 0;
			if ( (hlt>>14&1) || (hlt>>15&1) || (hlt>>41&1) || (hlt>>42&1))
      			trig_Mu17_Mu8 = 1;
    		else trig_Mu17_Mu8 = 0;

			vector<int> eleIndex;
			vector<int> selectedEle;
			passEleId(selectedEle);

			if(selectedEle.size()>1){
				
				if(eleCharge[selectedEle[0]]>eleCharge[selectedEle[1]]){
					pot=selectedEle[0];
					neg=selectedEle[1];
				}
				else{
					pot=selectedEle[1];
					neg=selectedEle[0];

				}

				lept0_pt=elePt[neg];
				lept0_eta=eleEta[neg];
				lept0_phi=elePhi[neg];

				lept1_pt=elePt[pot];
				lept1_eta=eleEta[pot];
				lept1_phi=elePhi[pot];
				
				lept0_charge=eleCharge[neg];
				lept1_charge=eleCharge[pot];

				ele1.SetPtEtaPhiM(lept0_pt, lept0_eta, lept0_phi, 0.000511);
				ele2.SetPtEtaPhiM(lept1_pt, lept1_eta, lept1_phi, 0.000511);
				Z=ele1+ele2;
				z_mass	= Z.M();
				if(z_mass<50.||z_mass>120.) continue;
				z_y 	= Z.Rapidity();
				z_pt	= Z.Pt();
			    z_eta 	= Z.Eta();
      			z_phi 	= Z.Phi();
				if(isMCReco) SF=getEleSF(lept0_pt,lept0_eta)*getEleSF(lept1_pt,lept1_eta);
				z_charge=eleCharge[neg]+eleCharge[pot];
				leptType=11;

				ZtoRest(ele1,ele2,Z);
				genWe(data1);
				OT->Fill();
				/*
				vector<int> selectedJet;
				passJetIdE(selectedJet,selectedEle[0],selectedEle[1]);
				if(selectedJet.size()>1){
					
					jet1.SetPtEtaPhiE(jetPt[selectedJet[0]],jetEta[selectedJet[0]],jetPhi[selectedJet[0]],jetEn[selectedJet[0]]);
					jet2.SetPtEtaPhiE(jetPt[selectedJet[1]],jetEta[selectedJet[1]],jetPhi[selectedJet[1]],jetEn[selectedJet[1]]);
					jets=jet1+jet2;
					
					genWe();
					z_mass=ZM;
					z_pt=Z.Pt();
					jets_pt=jets.Pt();
					OT->Fill();
				}
				*/
		}
	}
}
void mumuToZ(string path,TTree* OT,bool isMCReco){
	TreeReader data2(TString(path), "ggNtuplizer/EventTree");
	Long64_t ev2=data2.GetEntriesFast();
	double	ZM;
	TLorentzVector Mu1, Mu2, Z, jet1, jet2, jets;
	int neg,pot;
	for (Long64_t ev = 0; ev < ev2; ev++) {
		    data2.GetEntry(ev);
			readggtree(data2);
	    	if ( (hlt>>40&1) || (hlt>>5&1) ) trig_Ele23_Ele12 = 1;
    		else trig_Ele23_Ele12 = 0;
			if ( (hlt>>14&1) || (hlt>>15&1) || (hlt>>41&1) || (hlt>>42&1))
      			trig_Mu17_Mu8 = 1;
    		else trig_Mu17_Mu8 = 0;
			vector<int> eleIndex;
			vector<int> selectedMu;
			passMuonId(selectedMu);

			if(selectedMu.size()>1&&muPt[0]>20.){
				
				if(muCh[selectedMu[0]]>muCh[selectedMu[1]]){
					pot=selectedMu[0];
					neg=selectedMu[1];
				}
				else{
					pot=selectedMu[1];
					neg=selectedMu[0];

				}
//				if(muCh[pot]!=1) cout<<"Error";
//				if(muCh[neg]!=-1) cout<<"Error neg :"<<muCh[neg]<<endl;
				lept0_pt=muPt[neg];
				lept0_eta=muEta[neg];
				lept0_phi=muPhi[neg];

				lept1_pt=muPt[pot];
				lept1_eta=muEta[pot];
				lept1_phi=muPhi[pot];
				
				lept0_charge=muCh[neg];
				lept1_charge=muCh[pot];

				Mu1.SetPtEtaPhiM(lept0_pt, lept0_eta, lept0_phi, 0.104);
				Mu2.SetPtEtaPhiM(lept1_pt, lept1_eta, lept1_phi, 0.104);

				Z=Mu1+Mu2;
				z_mass	= Z.M();
				if(z_mass<50.&&z_mass>120.) continue;
				z_y 	= Z.Rapidity();
				z_pt	= Z.Pt();
			    z_eta 	= Z.Eta();
      			z_phi 	= Z.Phi();
				if(isMCReco) SF=getMuSF(lept0_pt,lept0_eta)*getMuSF(lept1_pt,lept1_eta);
				z_charge=muCh[pot]+muCh[neg];
				leptType=22;
				ZtoRest(Mu1,Mu2,Z);
				genWe(data2);
				OT->Fill();
				/*
				vector<int> selectedJet;
				passJetIdM(selectedJet,selectedMu[0],selectedMu[1]);
				if(selectedJet.size()>1){
					
					jet1.SetPtEtaPhiE(jetPt[selectedJet[0]],jetEta[selectedJet[0]],jetPhi[selectedJet[0]],jetEn[selectedJet[0]]);
					jet2.SetPtEtaPhiE(jetPt[selectedJet[1]],jetEta[selectedJet[1]],jetPhi[selectedJet[1]],jetEn[selectedJet[1]]);
					jets=jet1+jet2;

					genWe();
					z_mass=ZM;
					z_pt=Z.Pt();
					jets_pt=jets.Pt();
					OT->Fill();
				}
				*/
		}
	}
}

Long64_t CountE(string path){
	TreeReader data2(TString(path), "ggNtuplizer/EventTree"); 
	Long64_t ev2=data2.GetEntriesFast();
	float i;
	Long64_t N=0;
	for(Long64_t ev=0;ev<ev2;ev++){
		    data2.GetEntry(ev);
		i=data2.GetFloat("genWeight");
		if(i>0){
			N++;
		}
		else if(i<0){
			N--;
		}
	}
	cout<<endl<<N<<endl;
	return N;

}
