#ifndef MZ_h
#define MZ_h
#include <iostream>
#include <fstream>

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TVector2.h>
#include <TF1.h>
#include <TMath.h>
#include "Dir.h"
#include "twoL.h"
#include "untuplizer.h"
#include "tree.h"
#include <vector>
#include "jets.h"
using namespace std;
void EZ(string ggDir,string Ndir){

	TString outP ="~/GoChip01/";
	outP+=Ndir;
	outP+="(E).root";	

	TFile *MyFile = new TFile(outP,"RECREATE");
	TH1* hM = new TH1D("hM", "Two-Ele invariant mass", 90,0, 140);
	TH1* hM2 = new TH1D("hM2", "Two-Ele invariant mass", 90,0, 140);
	TH1* hJetMMu = new TH1D("hJetMMu", "B jet invariant mass(ZtoEE)",100,0, 15);
	TH1* ZvBM = new TH1D("ZvBM", "b jets/Z Pt(ZtoEE)", 200,0,10);
	h2JvZPtE->Reset("ICESM");	

	string mDir = ggDir+Ndir+"/";
	cout<<mDir<<endl;
	vector<string> mFile;
	getDir(mDir,mFile);
//		for(int i=0;i<30;i++){
		for(int i=0;i<mFile.size();i++){
			cout<<i<<" E in "<<mFile.size()<<endl;
			eeToZ(mDir+mFile[i],hM,hM2,hJetMMu,ZvBM);
	
		}
	hM->Write();
	hM2->Write();
	ZvBM->Write();
	hJetMMu->Write();
	h2JvZPtE->Write();
	MyFile->Close();
	
}
void MZ(string ggDir,string Ndir){

	TString outP ="~/GoChip01/";
	outP+=Ndir;
	outP+="(M).root";	

	TFile *MyFile = new TFile(outP,"RECREATE");
	TH1* hM = new TH1D("hM", "Two-Moun invariant mass", 90,0, 140);
	TH1* hM2 = new TH1D("hM2", "Two-Moun invariant mass", 90,0, 140);
	TH1* hJetMMu = new TH1D("hJetMMu", "B jet invariant mass(ZtoMuMu)",100,0, 15);
	TH1* ZvBM = new TH1D("ZvBM", "b jets/Z Pt(ZtoMuMu)", 200,0,10);
	h2JvZPtM->Reset("ICESM");	

	string mDir = ggDir+Ndir+"/";
	cout<<mDir<<endl;
	vector<string> mFile;
	getDir(mDir,mFile);
//		for(int i=0;i<15;i++){
		for(int i=0;i<mFile.size();i++){
			cout<<i<<" M in "<<mFile.size()<<endl;
			mumuToZ(mDir+mFile[i],hM,hM2,hJetMMu,ZvBM);
	
		}
	hM->Write();
	hM2->Write();
	ZvBM->Write();
	hJetMMu->Write();
	h2JvZPtM->Write();
	MyFile->Close();
	
}

void EZT(string ggDir,string Ndir,bool isMCReco=false){
	
	TString outP ="~/ToChip01/tinyTree/";
	outP+=Ndir;
	outP+="_DY_Tree_E.root";	


	TFile *MyFile = new TFile(outP,"RECREATE");
	TTree *OT = new TTree("OT","EventTree");

	inittree(OT);
	initPUWCal();

	string mDir = ggDir+Ndir+"/";
	cout<<mDir<<endl;
	vector<string> mFile;
	getDir(mDir,mFile);
//		for(int i=0;i<10;i++){
		for(int i=0;i<mFile.size();i++){
			cout<<i<<" E in "<<mFile.size()<<endl;
			eeToZ(mDir+mFile[i],OT,isMCReco);
	
		}
	OT->Write();
	MyFile->Close();
	
}

void MZT(string ggDir,string Ndir,bool isMCReco=false){
	
	TString outP ="~/ToChip01/tinyTree/";
	outP+=Ndir;
	outP+="_DY_Tree_M.root";	

	TFile *MyFile = new TFile(outP,"RECREATE");
	TTree *OT = new TTree("OT","EventTree");
	inittree(OT);
	initPUWCal();
	string mDir = ggDir+Ndir+"/";
	cout<<mDir<<endl;
	vector<string> mFile;
	getDir(mDir,mFile);
//		for(int i=0;i<10;i++){
		for(int i=0;i<mFile.size();i++){
			cout<<i<<" M in "<<mFile.size()<<endl;
			mumuToZ(mDir+mFile[i],OT,isMCReco);
		}

	OT->Write();
	MyFile->Close();
	
}
void CT(string mDir){
	cout<<mDir<<endl;
	vector<string> mFile;
	getDir(mDir,mFile);
	Long64_t Ent=0;
		for(int i=0;i<mFile.size();i++){
			cout<<i<<" CT in "<<mFile.size()<<endl;
			Ent+=CountE(mDir+mFile[i]);
			cout<<Ent<<endl;
		}

	
}
#endif
