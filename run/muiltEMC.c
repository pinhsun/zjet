#include "SimThr.h"
#include "MZ.h"
#include <unistd.h>
string Mis1[] = {
	        "job_DoubleMu_Run2016B_Legacy",
	        "job_DoubleMu_Run2016C_Legacy",
	        "job_DoubleMu_Run2016D_Legacy",
	        "job_DoubleMu_Run2016E_Legacy",
	        "job_DoubleMu_Run2016F_Legacy",
	        "job_DoubleMu_Run2016G_Legacy",
	        "job_DoubleMu_Run2016H_Legacy",
			"job_DoubleMu_Run2017B_MarReminiAOD",
			"job_DoubleMu_Run2017C_MarReminiAOD",
			"job_DoubleMu_Run2017D_MarReminiAOD",
			"job_DoubleMu_Run2017E_MarReminiAOD",
			"job_DoubleMu_Run2017F_MarReminiAOD",
			"job_DoubleMu_Run2018A_EarlyRereco",
			"job_DoubleMu_Run2018B_EarlyRereco",
			"job_DoubleMu_Run2018C_EarlyRereco",
			"job_DoubleMu_Run2018D_PR",

			};
string Mis2[]={
			"job_fall17_TT_aMCatNLO",
			"job_summer16_TT_powheg",
			"job_summer16_TT_aMCatNLO",
			"job_autumn18_TT_aMCatNLO",
			};
string Mis3[]={
			"job_DoubleEG_Run2016B_Legacy",
			"job_DoubleEG_Run2016C_Legacy",
			"job_DoubleEG_Run2016D_Legacy",
			"job_DoubleEG_Run2016E_Legacy",
			"job_DoubleEG_Run2016F_Legacy",
			"job_DoubleEG_Run2016G_Legacy",
			"job_DoubleEG_Run2016H_Legacy",
			"job_DoubleEG_Run2017B_MarReminiAOD",
			"job_DoubleEG_Run2017C_MarReminiAOD",
			"job_DoubleEG_Run2017D_MarReminiAOD",
			"job_DoubleEG_Run2017E_MarReminiAOD",
			"job_DoubleEG_Run2017F_MarReminiAOD",
			"job_EGamma_Run2018A_EarlyRereco",
			"job_EGamma_Run2018B_EarlyRereco",
			"job_EGamma_Run2018C_EarlyRereco",
			"job_EGamma_Run2018D_22Jan2019",
			};

string Dir="/data6/ggNtuples/V09_04_13_04/";
string Dir2="/data6/ggNtuples/V10_02_10_04/";
string Dir3="/data1/ggNtuples/V09_04_13_03/";
string Dir4="/data3/ggNtuples/V10_02_10_04/";

const int MissLen=4;
string in="MC.log";
void muiltEMC(){
//CountE("/data6/ggNtuples/V09_04_13_04/job_summer16_DYJetsToLL_m50_aMCatNLO_ext2/");
//initThr(in);

initSF();
initThr("data.log");
		Mis=7;
	while(1){
		ThrNext();
		if(Mis>=8) break;
		if(Mis<7){
	//	EZT(Dir3,Mis3[Mis]);
		MZT(Dir3,Mis1[Mis]);
		}else{
	//		EZT("/data6/ggNtuples/V09_04_13_04/","job_summer16_DYJetsToLL_m50_aMCatNLO_ext2",true);
			MZT("/data6/ggNtuples/V09_04_13_04/","job_summer16_DYJetsToLL_m50_aMCatNLO_ext2",true);
		}
		Mis++;

	}
	
}
