#ifndef ElectronSelection_h__
#define ElectronSelection_h__


void passEleId(std::vector<int> &selectedEle) {
  selectedEle.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> elePtUnsorted;
  elePtUnsorted.clear();

  for (int i = 0; i < nEle; i++) {
    if (elePt[i] < 10.) continue;
    if (fabs(eleSCEta[i]) > 2.5) continue;
    if (fabs(eleSCEta[i]) > 1.4442 && fabs(eleSCEta[i]) < 1.566) continue;
    if ( ((eleID[i] >> 3) &1) == 1 ) {
      passUnsorted.push_back(i);
      elePtUnsorted.push_back(elePt[i]);
    }
  }

  //sort ele in pt descending
  int siz = (int) passUnsorted.size();
  if (siz < 1) return;

  int ind[siz];
  TMath::Sort(siz, &elePtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedEle.push_back(passUnsorted[ind[i]]);
  }

}


#endif
