#include "TH1.h"
#include "TH2.h"

TFile *feleRecoSF = new TFile("external/EGM2D_BtoH_GT20GeV_RecoSF_Legacy2016.root", "READ");
TFile *feleSF = new TFile("external/2016LegacyReReco_ElectronTight_Fall17V2.root", "READ");

TFile *fmuSF_BF = new TFile("external/RunBCDEF_SF_ID_Muon.root", "READ");
TFile *fmuSF_GH = new TFile("external/RunGH_SF_ID_Muon.root", "READ");

TFile *fmuSFIso_BF = new TFile("external/RunBCDEF_SF_ISO_Muon.root", "READ");
TFile *fmuSFIso_GH = new TFile("external/RunGH_SF_ISO_Muon.root", "READ");

//get histogram of SFs from files 
TH2F *HeleReco = (TH2F*) feleRecoSF->Get("EGamma_SF2D");
TH2F *HeleSF   = (TH2F*) feleSF->Get("EGamma_SF2D");

TH2F *HmuSF_BF = (TH2F*) fmuSF_BF->Get("NUM_TightID_DEN_genTracks_eta_pt");
TH2F *HmuSF_GH = (TH2F*) fmuSF_GH->Get("NUM_TightID_DEN_genTracks_eta_pt");

TH2F *HmuISO_BF = (TH2F*) fmuSFIso_BF->Get("NUM_TightRelIso_DEN_TightIDandIPCut_eta_pt");
TH2F *HmuISO_GH = (TH2F*) fmuSFIso_GH->Get("NUM_TightRelIso_DEN_TightIDandIPCut_eta_pt");

float maxPt_eleReco;
float maxPt_eleSF;
float maxPt_muISO_BF;
float maxPt_muISO_GH;
float maxPt_muSF_BF;
float maxPt_muSF_GH;

float GetMaxPT(TH2F* h){

	int NBinsY = h->GetNbinsY();
	return h->GetYaxis()->GetBinCenter(NBinsY);
}
void initSF(){

	maxPt_eleReco	= GetMaxPT(HeleReco);	
    maxPt_eleSF		= GetMaxPT(HeleSF);	
    maxPt_muISO_BF	= GetMaxPT(HmuISO_BF);	
    maxPt_muISO_GH	= GetMaxPT(HmuISO_GH);	
    maxPt_muSF_BF	= GetMaxPT(HmuSF_BF);
    maxPt_muSF_GH	= GetMaxPT(HmuSF_GH);
	
}
int binx, biny;
float GetBin(TH2F *h,float pt,float eta){
	biny = h->GetYaxis()->FindBin(pt);
	binx = h->GetXaxis()->FindBin(eta);
	return h->GetBinContent(binx, biny);
}

float GetBin(TH2F *h,float pt,float eta,float maxPt){
	if(pt<20.) pt=20.;
	if(pt>maxPt) pt=maxPt;
	biny = h->GetYaxis()->FindBin(pt);
	binx = h->GetXaxis()->FindBin(eta);
	return h->GetBinContent(binx, biny);
}

float eleRecoSF;
float eleSF;
float getEleSF(float pt, float eta){
	eleRecoSF	= GetBin(HeleReco,pt,eta,maxPt_eleReco);
	eleSF 	  	= GetBin(HeleSF,pt,eta,maxPt_eleSF);
	return eleRecoSF * eleSF;
}

float muISO_BF;
float muISO_GH;
float muSF_BF;
float muSF_GH;

float getMuSF(float pt,float eta){

	muISO_BF = GetBin(HmuISO_BF, pt,eta,maxPt_muISO_BF);  
	muISO_GH = GetBin(HmuISO_GH, pt,eta,maxPt_muISO_GH);
	muSF_BF  = GetBin(HmuSF_BF , pt,eta,maxPt_muSF_BF);
	muSF_GH  = GetBin(HmuSF_GH , pt,eta,maxPt_muSF_GH);
	return (muISO_BF*0.55+muISO_GH*0.45)*(muSF_BF*0.55+muISO_GH*0.45);

}
