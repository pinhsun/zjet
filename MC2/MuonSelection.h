#ifndef MuonSelection_h__
#define MuonSelection_h__

//#include "xAna.h"

open("/data")
void passMuonId (std::vector<int> &selectedMu) {
  selectedMu.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> muPtUnsorted;
  muPtUnsorted.clear();

  for (int i = 0; i < nMu; i++) {
    if (muPt[i] < 10.) continue;
    if (fabs(muEta[i]) > 2.4) continue;
    if ( ((muIDbit[i] >> 2) &1) != 1) continue;
    if ( (muPFChIso[i] + TMath::Max(0., muPFPhoIso[i] + muPFNeuIso[i] -0.5 * muPFPUIso[i]))/muPt[i] > 0.15) continue;
    //if ( (muPFChIso[i] + TMath::Max(0., muPFPhoIso[i] + muPFNeuIso[i] -0.5 * muPFPUIso[i]))/muPt[i] > 0.25) continue;
    passUnsorted.push_back(i);
    muPtUnsorted.push_back(muPt[i]);
  }

  //sort pt in descending
  int siz = (int) passUnsorted.size();
  if (siz < 1) return;

  int ind[siz];
  TMath::Sort(siz, &muPtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedMu.push_back(passUnsorted[ind[i]]);
  }

}

#endif
