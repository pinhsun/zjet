#ifndef M_Pi
#define M_Pi           3.14159265358979323846
#endif
#ifndef Select_h
#define Select_h
#include <TH1.h>
#include <TMath.h>
TH1* ToCloseJet = new TH1I("ToCloseJet","To Close Jet",11,-1,10);
float deltaR(float eta1, float phi1, float eta2, float phi2) {

  float dEta = eta1 - eta2;
  float dPhi = phi1 - phi2;
  if (dPhi > TMath::Pi()) dPhi -= 2*TMath::Pi();
  if (dPhi <= -TMath::Pi()) dPhi += 2* TMath::Pi();

  return sqrt(pow(dEta,2) + pow (dPhi,2));

}

bool ToCloseM(int m,int jet) {

  float dEta = muEta[m] - jetEta[jet];
  float dPhi = muPhi[m] - jetPhi[jet];
  if (dPhi > TMath::Pi()) dPhi -= 2*TMath::Pi();
  if (dPhi <= -TMath::Pi()) dPhi += 2* TMath::Pi();

  return (dEta*dEta+dPhi*dPhi)<0.16;

}
bool ToCloseE(int e,int jet) {

  float dEta = eleEta[e] - jetEta[jet];
  float dPhi = elePhi[e] - jetPhi[jet];
  if (dPhi > TMath::Pi()) dPhi -= 2*TMath::Pi();
  if (dPhi <= -TMath::Pi()) dPhi += 2* TMath::Pi();

  return (dEta*dEta+dPhi*dPhi)<0.16;

}
void passEleId(std::vector<int> &selectedEle) {
  selectedEle.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> elePtUnsorted;
  elePtUnsorted.clear();

  for (int i = 0; i < nMC; i++) {
	if (abs(mcPID[i])!=11 || ((mcStatusFlag[i]>>0)&1)!=1 ) continue;
   // if (elePt[i] < 10.) continue;
//    if (fabs(eleEta[i]) > 2.5) continue;
//    if (fabs(eleEta[i]) > 1.4442 && fabs(eleSCEta[i]) < 1.566) continue;
//    if ( ((eleID[i] >> 3) &1) == 1 ) {
      passUnsorted.push_back(i);
      elePtUnsorted.push_back(mcPt[i]);
    
  }

  //sort ele in pt descending
  int siz = (int) passUnsorted.size();
  if (siz < 1) return;

  int ind[siz];
  TMath::Sort(siz, &elePtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedEle.push_back(passUnsorted[ind[i]]);
  }

}



void passMuonId (std::vector<int> &selectedMu) {
  selectedMu.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> muPtUnsorted;
  muPtUnsorted.clear();

  for (int i = 0; i < nMC; i++) {
	if (abs(mcPID[i])!=13 || ((mcStatusFlag[i]>>0)&1)!=1 ) continue;
    passUnsorted.push_back(i);
    muPtUnsorted.push_back(mcPt[i]);
  }

  //sort pt in descending
  int siz = (int) passUnsorted.size();
  if (siz < 1) return;

  int ind[siz];
  TMath::Sort(siz, &muPtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedMu.push_back(passUnsorted[ind[i]]);
  }

}

void passJetIdM(std::vector<int> &selectedJet,int m1,int m2) {
  selectedJet.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> jetPtUnsorted;
  jetPtUnsorted.clear();
  int c=0;
  for (int i = 0; i < nJet; i++) {
    if (jetPt[i] < 25.) continue;
    if (fabs(jetEta[i]) > 2.5) continue;
	if (ToCloseM(m1,i)) {
		c++;
		continue;
	}
	
	if (ToCloseM(m2,i)) {
		c++;
		continue;
	}
    passUnsorted.push_back(i);
    jetPtUnsorted.push_back(jetPt[i]);
    
  }
  ToCloseJet->Fill(c);
  //sort ele in pt descending
  int siz = (int) passUnsorted.size();
  if (siz < 1)  return;

  int ind[siz];
  TMath::Sort(siz, &jetPtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedJet.push_back(passUnsorted[ind[i]]);
  }
}
void passJetIdE(std::vector<int> &selectedJet,int e1,int e2) {
  selectedJet.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> jetPtUnsorted;
  jetPtUnsorted.clear();
  int c=0;
  for (int i = 0; i < nJet; i++) {
    if (jetPt[i] < 25.) continue;
    if (fabs(jetEta[i]) > 2.5) continue;
	if (ToCloseE(e1,i)) {
		c++;
		continue;
	}
	
	if (ToCloseE(e2,i)) {
		c++;
		continue;
	}
    passUnsorted.push_back(i);
    jetPtUnsorted.push_back(jetPt[i]);
    
  }
  ToCloseJet->Fill(c);
  //sort ele in pt descending
  int siz = (int) passUnsorted.size();
  if (siz < 1)  return;

  int ind[siz];
  TMath::Sort(siz, &jetPtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedJet.push_back(passUnsorted[ind[i]]);
  }
}
void passJetId(std::vector<int> &selectedJet) {
  selectedJet.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> jetPtUnsorted;
  jetPtUnsorted.clear();

  for (int i = 0; i < nJet; i++) {
    if (jetPt[i] < 25.) continue;
    if (fabs(jetEta[i]) < 2.5){
      passUnsorted.push_back(i);
      jetPtUnsorted.push_back(jetPt[i]);
    }
  }

  //sort ele in pt descending
  int siz = (int) passUnsorted.size();
  if (siz < 1)  return;

  int ind[siz];
  TMath::Sort(siz, &jetPtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedJet.push_back(passUnsorted[ind[i]]);
  }

}

#endif
