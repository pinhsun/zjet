#ifndef tree_h
#define tree_h

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TMath.h>
#include <TLorentzVector.h>

#include <iostream>

const int maxPar = 50;
Int_t run;
Bool_t  isData;
Int_t lumis;
Long64_t event;

Int_t   trig_Ele23_Ele12;
Int_t   trig_Ele17_Ele12;
Int_t   trig_Mu17_Mu8;

Int_t 	leptType;
Float_t lept0_pt;
Float_t lept0_eta;
Float_t lept0_phi;

Float_t lept1_pt;
Float_t lept1_eta;
Float_t lept1_phi;

Float_t z_pt;
Float_t z_eta;
Float_t z_phi;
Float_t z_y;
Float_t z_mass;
Int_t   z_charge;

Float_t	lep0_theta_Zrest;		
Float_t lep0_costheta_Zrest;		
Float_t lep0_phi_Zrest;			
Float_t lep1_theta_Zrest;		
Float_t lep1_costheta_Zrest;		
Float_t lep1_phi_Zrest;			

Int_t	  _nJet;
Float_t	_jetPt;
Float_t	_jetEn;
Float_t jets_pt;
Float_t genWeight_;
Float_t PUWeight_69p2nb;

Int_t	lept0_charge;
Int_t	lept1_charge;

void inittree(TTree* tree) {

  tree->Branch("run",         &run);
  tree->Branch("isData",      &isData);
  tree->Branch("lumis",       &lumis);
  tree->Branch("event",       &event);

  tree->Branch("trig_Ele23_Ele12",	&trig_Ele23_Ele12);
  tree->Branch("trig_Ele17_Ele12",	&trig_Ele17_Ele12);
  tree->Branch("trig_Mu17_Mu8",		&trig_Mu17_Mu8);	

  tree->Branch("z_pt",		    &z_pt);
  tree->Branch("z_pt",		    &z_pt);
  tree->Branch("z_phi",		    &z_phi);
  tree->Branch("z_eta",		    &z_eta);
  tree->Branch("z_y",   		&z_y);	
  tree->Branch("z_mass",	    &z_mass);
  tree->Branch("z_charge",		&z_charge);

  tree->Branch("leptType",	    &leptType);
  tree->Branch("lept0_pt",	    &lept0_pt);
  tree->Branch("lept0_eta",	    &lept0_eta);
  tree->Branch("lept0_phi",	    &lept0_phi);

  tree->Branch("lept1_pt",	    &lept1_pt);
  tree->Branch("lept1_eta",	    &lept1_eta);
  tree->Branch("lept1_phi",	    &lept1_phi);

  tree->Branch("lep0_theta_Zrest",		&lep0_theta_Zrest);		
  tree->Branch("lep0_costheta_Zrest",	&lep0_costheta_Zrest);	
  tree->Branch("lep0_phi_Zrest",		&lep0_phi_Zrest);			
  tree->Branch("lep1_theta_Zrest",		&lep1_theta_Zrest);		
  tree->Branch("lep1_costheta_Zrest",	&lep1_costheta_Zrest);	
  tree->Branch("lep1_phi_Zrest",		&lep1_phi_Zrest);			

  tree->Branch("jets_pt",     		&jets_pt);
  tree->Branch("genWeight",	  		&genWeight_);	
  tree->Branch("PUWeight_69p2nb",	&PUWeight_69p2nb);

  tree->Branch("lept0_charge",	&lept0_charge);
  tree->Branch("lept1_charge",	&lept1_charge);

}

bool	isMc;
bool	hasMu;
ULong64_t hlt;
Float_t genWeight;

Int_t nEle ;
Int_t* eleCharge;
Int_t* eleChargeConsistent;
Float_t* eleEn;
Float_t* elePt ;
Float_t* eleEta ;
Float_t* elePhi ;
Float_t* eleSCEta ;
Float_t* eleSCPhi;
Float_t* eleSCEtaWidth;
Float_t* eleSCPhiWidth;
Float_t* eleSCEn ;
Short_t* eleID;


Int_t    nMu;
Int_t*   muType;
Float_t* muPt;
Float_t* muEta;
Float_t* muPhi;
Int_t*   muCh;
Float_t* muPFChIso;
Float_t* muPFPhoIso;
Float_t* muPFNeuIso;
Float_t* muPFPUIso;
Float_t* muPFChIso03;
Float_t* muPFPhoIso03;
Float_t* muPFNeuIso03;
Float_t* muPFPUIso03;
Int_t* muIDbit;

//jet
Int_t nJet;
Float_t* jetPt;
Float_t* jetEn;
Float_t* jetEta;
Float_t* jetPhi;
Float_t* jetRawPt;
Float_t* jetRawEn;
Float_t* jetMt;
Float_t* jetArea;
Int_t* jetID;   

//gen level
Int_t nMC;
Int_t* mcPID;
Int_t* mcMomPID;
Int_t* mcGMomPID;
Float_t* mcPt;
Float_t* mcEt;
Float_t* mcPhi;
Float_t* mcEta;
Float_t* mcMomPt;
Float_t* mcMomEta;
Float_t* mcMomPhi;
Float_t* mcMomMass;
UShort_t* mcStatusFlag;
Float_t* mcCalIsoDR03;
Float_t* mcCalIsoDR04;
Float_t genPho1;
Float_t genEle1;
Float_t genEle2;
Float_t genMu1;
Float_t genMu2;
Float_t genEleEta1;
Float_t genEleEta2;
Float_t genMuEta1;
Float_t genMuEta2;
Float_t genPhoEta1;
Float_t genElePhi1;
Float_t genElePhi2;
Float_t genMuPhi1;
Float_t genMuPhi2;
Float_t genPhoPhi1;




void readggtree(TreeReader &data) {

  run = data.GetInt("run");
  event = data.GetLong64("event");
  lumis = data.GetInt("lumis");
  hlt = data.GetLong64("HLTEleMuX");

  nMC = data.GetInt("nMC");
  mcPID = data.GetPtrInt("mcPID");
  //eleCharge = data.GetPtrInt("eleCharge");
  mcPt = data.GetPtrFloat("mcPt");
//  eleEn = data.GetPtrFloat("eleCalibEn");
  mcEta = data.GetPtrFloat("mcEta");
  mcPhi = data.GetPtrFloat("mcPhi");
  mcStatusFlag = (UShort_t*) data.GetPtrShort("mcStatusFlag");
  /*
  nEle = data.GetInt("nEle");
  mcPID = data.GetPtrInt("mcPID");
  eleCharge = data.GetPtrInt("eleCharge");
  elePt = data.GetPtrFloat("eleCalibPt");
  eleEn = data.GetPtrFloat("eleCalibEn");
  eleEta = data.GetPtrFloat("eleEta");
  elePhi = data.GetPtrFloat("elePhi");
  
  eleSCEta = data.GetPtrFloat("eleSCEta");
  eleSCPhi = data.GetPtrFloat("eleSCPhi");
  eleSCEtaWidth = data.GetPtrFloat("eleSCEtaWidth");
  eleSCPhiWidth = data.GetPtrFloat("eleSCPhiWidth");
  eleSCEn = data.GetPtrFloat("eleSCEn");
  eleID = data.GetPtrShort("eleIDbit");

  nMu = data.GetInt("nMu");
  muType = data.GetPtrInt("muType");
  muPt = data.GetPtrFloat("muPt");//
  muEta = data.GetPtrFloat("muEta");
  muPhi = data.GetPtrFloat("muPhi");
  muCh = data.GetPtrInt("muCharge");
  muPFChIso = data.GetPtrFloat("muPFChIso");
  muPFNeuIso = data.GetPtrFloat("muPFNeuIso");
  muPFPhoIso = data.GetPtrFloat("muPFPhoIso");
  muPFPUIso = data.GetPtrFloat("muPFPUIso");
  muPFChIso03 = data.GetPtrFloat("muPFChIso03");
  muPFNeuIso03 = data.GetPtrFloat("muPFNeuIso03");
  muPFPhoIso03 = data.GetPtrFloat("muPFPhoIso03");
  muPFPUIso03 = data.GetPtrFloat("muPFPUIso03");
  muIDbit =  data.GetPtrInt("muIDbit");

  nJet = data.GetInt("nJet");
  jetPt		= data.GetPtrFloat("jetPt");	
  jetEn		= data.GetPtrFloat("jetEn");	
  jetEta	= data.GetPtrFloat("jetEta");
  jetPhi	= data.GetPtrFloat("jetPhi");	
  jetRawPt	= data.GetPtrFloat("jetRawPt");	
  jetRawEn	= data.GetPtrFloat("jetRawEn");	
  jetMt		= data.GetPtrFloat("jetMt");	
  jetArea	= data.GetPtrFloat("jetArea");
  jetID		= data.GetPtrInt("jetID");		
*/
	
  if (data.HasMC()){
	  isMc		= true;
	  isData	= false;
	  genWeight = data.GetFloat("genWeight");
  }else{
	  isData	= true;
  	  isMc		= false;
	  genWeight = 0;
  }
}

Float_t z_phi_;
Float_t Zlab_pz;
TVector3 vec3_BoostToRest;
TVector3 vec3_BoostToPz, vec3_BoostToPt;

void ZtoRest(TLorentzVector& lep1,TLorentzVector& lep2,TLorentzVector& Zlab){
    z_phi_ = Zlab.Phi();
    Zlab_pz = Zlab.Pz();

    //TLorentzVector vec4_Everything;
    //vec4_Everything . RotateZ (-Zlab.Phi());
    //cout << "Zlab phi before boost: " << Zlab.Phi() << endl;

    //cout << "Zlab phi after boost: " << Zlab.Phi() << endl;

    //TVector3 vec3_BoostToPz(0., 0., Zlab.Pz()/Zlab.E());
    //TVector3 vec3_BoostToPt(Zlab.Px()/Zlab.E(), Zlab.Py()/Zlab.E(), 0.);

    vec3_BoostToRest = Zlab.BoostVector();
    vec3_BoostToPz.SetX(0.);
    vec3_BoostToPz.SetY(0.);
    vec3_BoostToPz.SetZ(vec3_BoostToRest.Z());

    //boost to Zpz first
    lep1.Boost(-vec3_BoostToPz);
    lep2.Boost(-vec3_BoostToPz);
    Zlab.Boost(-vec3_BoostToPz);
    //cout << "Pz of Z: " << Zlab.Pz() << endl;

    lep1.RotateZ (-z_phi_);
    lep2.RotateZ (-z_phi_);
    Zlab.RotateZ (-z_phi_);


    //then boost to Zpt
    vec3_BoostToPt = Zlab.BoostVector();
    Zlab.Boost(-vec3_BoostToPt);
    lep1.Boost(-vec3_BoostToPt);
    lep2.Boost(-vec3_BoostToPt);

    //cout << "Z px: " << Zlab.Px() << "  py : " << Zlab.Py() << endl;
    //TVector3 vec3_Boost;
    //vec3_Boost = Zlab.BoostVector();

    
    lep0_theta_Zrest	= lep1.Theta();
    lep0_costheta_Zrest = lep1.CosTheta();
    lep0_phi_Zrest 		= lep1.Phi();
    lep1_theta_Zrest	= lep2.Theta();
    lep1_costheta_Zrest = lep2.CosTheta();
    lep1_phi_Zrest 		= lep2.Phi();

}

#endif
