#ifndef SimThr_h
#define SimThr_h
#include<string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include<sys/file.h>
int ThrNum, Mis,allThrNum;
FILE * fp;
int fd;
bool ThrInit_bo=1;
string f;
int initThr(string path)
{
   f=path;
   fp = fopen (f.c_str(), "r+");
   fd=fileno(fp);
   flock(fd,LOCK_EX);
   int i=fscanf(fp,"%d  %d  ",&Mis,&ThrNum);
   
   if(i==-1){
	   fputs("0  1  ",fp);
   }
   printf("Thread: %d\n",ThrNum+1);
   rewind(fp);
   fprintf(fp,"%d  %d  ",Mis,ThrNum+1);
   flock(fd,LOCK_UN);
   ThrInit_bo=0;
   fclose(fp);
   return(0);
}
int ThrNext(){
   if(ThrInit_bo) initThr("Tmp.ms");
   fp = fopen (f.c_str(), "r+");
   fd=fileno(fp);
   flock(fd,LOCK_EX);
   
   int i=fscanf(fp,"%d  %d  ",&Mis,&allThrNum);
   
   rewind(fp);
   fprintf(fp,"%d  %d  ",Mis+1,allThrNum);
   flock(fd,LOCK_UN);
   fclose(fp);
   return 0;
}
#endif
