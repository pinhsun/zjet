#ifndef tree_h
#define tree_h

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TMath.h>
#include <TLorentzVector.h>

#include <iostream>


const int maxPar = 50;

Int_t run_;
Bool_t  isData_;
Int_t lumis_;
Long64_t event_;

Int_t   trig_Ele23_Ele12_;
Int_t   trig_Ele17_Ele12_;
Int_t   trig_Mu17_Mu8_;

Int_t 	leptType_;
Float_t lept0_pt_;
Float_t lept0_eta_;
Float_t lept0_phi_;

Float_t lept1_pt_;
Float_t lept1_eta_;
Float_t lept1_phi_;

Float_t z_pt_;
Float_t z_eta_;
Float_t z_phi_;
Float_t z_y_;
Float_t z_mass_;
Int_t   z_charge_;

Float_t	lep0_theta_Zrest_;		
Float_t lep0_costheta_Zrest_;		
Float_t lep0_phi_Zrest_;			
Float_t lep1_theta_Zrest_;		
Float_t lep1_costheta_Zrest_;		
Float_t lep1_phi_Zrest_;			

Int_t	  _nJet_;
Float_t	_jetPt_;
Float_t	_jetEn_;
Float_t jets_pt_;
Float_t genWeight_;
Float_t PUWeight_69p2nb_;

void inittree(TTree* tree) {

  tree->Branch("run",         &run_);
  tree->Branch("isData",      &isData_);
  tree->Branch("lumis",       &lumis_);
  tree->Branch("event",       &event_);

  tree->Branch("trig_Ele23_Ele12",	&trig_Ele23_Ele12_);
  tree->Branch("trig_Ele17_Ele12",	&trig_Ele17_Ele12_);
  tree->Branch("trig_Mu17_Mu8",		&trig_Mu17_Mu8_);	

  tree->Branch("z_pt",		    &z_pt_);
  tree->Branch("z_pt",		    &z_pt_);
  tree->Branch("z_phi",		    &z_phi_);
  tree->Branch("z_eta",		    &z_eta_);
  tree->Branch("z_y",   		&z_y_);	
  tree->Branch("z_mass",	    &z_mass_);

  tree->Branch("leptType",	    &leptType_);
  tree->Branch("lept0_pt",	    &lept0_pt_);
  tree->Branch("lept0_eta",	    &lept0_eta_);
  tree->Branch("lept0_phi",	    &lept0_phi_);

  tree->Branch("lept1_pt",	    &lept1_pt_);
  tree->Branch("lept1_eta",	    &lept1_eta_);
  tree->Branch("lept1_phi",	    &lept1_phi_);

  tree->Branch("lep0_theta_Zrest",		&lep0_theta_Zrest_);		
  tree->Branch("lep0_costheta_Zrest",	&lep0_costheta_Zrest_);	
  tree->Branch("lep0_phi_Zrest",		&lep0_phi_Zrest_);			
  tree->Branch("lep1_theta_Zrest",		&lep1_theta_Zrest_);		
  tree->Branch("lep1_costheta_Zrest",	&lep1_costheta_Zrest_);	
  tree->Branch("lep1_phi_Zrest",		&lep1_phi_Zrest_);			

  tree->Branch("jets_pt",     		&jets_pt_);
  tree->Branch("genWeight",	  		&genWeight_);	
  tree->Branch("PUWeight_69p2nb",	&PUWeight_69p2nb_);	

}

Int_t 	run;
Bool_t  	isData;
Int_t 	lumis;
Long64_t event;

Int_t   	trig_Ele23_Ele12;
Int_t   	trig_Ele17_Ele12;
Int_t   	trig_Mu17_Mu8;

Int_t 	leptType;
Float_t 	lept0_pt;
Float_t 	lept0_eta;
Float_t 	lept0_phi;

Float_t 	lept1_pt;
Float_t 	lept1_eta;
Float_t 	lept1_phi;

Int_t  	lept0_charge;
Int_t  	lept1_charge;

Float_t 	z_pt;
Float_t 	z_eta;
Float_t 	z_phi;
Float_t 	z_y;
Float_t 	z_mass;
Int_t   	z_charge;

Float_t  lep0_theta_Zrest;		
Float_t  lep0_costheta_Zrest;		
Float_t 	lep0_phi_Zrest;			
Float_t  lep1_theta_Zrest;		
Float_t  lep1_costheta_Zrest;		
Float_t  lep1_phi_Zrest;			

Float_t 	genWeight;
Float_t	PUWeight_69p2nb;    	

void readggtree(TreeReader &data) {

  run = data.GetInt("run");
  event = data.GetLong64("event");
  isData =data.GetBool("isData");
  lumis = data.GetInt("lumis");

  trig_Ele23_Ele12	=	data.GetInt("trig_Ele23_Ele12");
  trig_Ele17_Ele12	=	data.GetInt("trig_Ele17_Ele12");
  trig_Mu17_Mu8		=	data.GetInt("trig_Mu17_Mu8");
                    
  leptType			=	data.GetInt("leptType");
  lept0_pt			=	data.GetFloat("lept0_pt");
  lept0_eta			=	data.GetFloat("lept0_eta");
  lept0_phi			=	data.GetFloat("lept0_phi");
                     
  lept1_pt			=	data.GetFloat("lept1_pt");
  lept1_eta			=	data.GetFloat("lept1_eta");
  lept1_phi			=	data.GetFloat("lept1_phi");
                                              
//  lept0_charge		=	data.GetInt("lept0_charge");
//  lept1_charge		=	data.GetInt("lept1_charge");

  z_pt				=	data.GetFloat("z_pt");
  z_eta				=	data.GetFloat("z_eta");
  z_phi				=	data.GetFloat("z_phi");                    
  z_y					=	data.GetFloat("z_y");
  z_mass				=	data.GetFloat("z_mass");
  z_charge			=	data.GetInt("z_charge");
                     
  lep0_theta_Zrest		=	data.GetFloat("lep0_theta_Zrest");	  	 
  lep0_costheta_Zrest	=	data.GetFloat("lep0_costheta_Zrest");
  lep0_phi_Zrest			=	data.GetFloat("lep0_phi_Zrest");	  		 
  lep1_theta_Zrest		=	data.GetFloat("lep1_theta_Zrest");	  	 
  lep1_costheta_Zrest	=	data.GetFloat("lep1_costheta_Zrest");
  lep1_phi_Zrest			=	data.GetFloat("lep1_phi_Zrest");	  		 
                                   
  genWeight					=	data.GetFloat("genWeight");
  PUWeight_69p2nb			=	data.GetFloat("PUWeight_69p2nb");    	

}

#endif
