#include <sstream>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TCanvas.h>
#include <iostream>
#include <string>
#include <vector>
#include "untuplizer.h"
#include "tree.h"
using namespace std;

TCanvas* GobalCanvas;
float GobalWeight=0;
float Lumi2016 =0;
int CdAmount;
int NowCd=0;
void drawDataMC(TH1* data){
	if(NowCd==CdAmount) return;
	NowCd++;
	GobalCanvas->cd(NowCd);
	cout<<NowCd<<endl;
//	MC->Draw("PFC");
	data->Draw();

}
Long64_t TotalN=0;
float weight;
bool NotPass(int typeInt){
	if(z_mass>100.||z_mass<80.) return true;

	if(z_charge!=0) return true;
//	if(lept0_charge!=-11) return true;
//	if(lept1_charge!=11) return true;

	if(typeInt==11){
		if(!trig_Ele23_Ele12) return true;

	}else if(typeInt==22){
		if(!trig_Mu17_Mu8) return true;
	}


	if(lept0_pt>lept1_pt){
		if(lept0_pt<25.) return true;
		if(lept1_pt<20.) return true;
	}else{
		if(lept0_pt<20.) return true;
		if(lept1_pt<25.) return true;

	}
	if(genWeight>-0.1){
		TotalN+=1;
	}else{
		TotalN-=1;
	}
	if(isData){
		weight = 1.;
	}
	else{
		weight = PUWeight_69p2nb;
	}
	return false;
}
struct HistoArray{
	int len;
	float boundary[50];
	vector<TH1*> Arr;
};
void InitHistoArray(HistoArray& t,string name,int n,float* arr,int bins,float down,float up){
	t.len=n;
	TString tm  =";";
	t.boundary[0]=arr[0];
	for(int i=1;i<=n;i++){
		t.boundary[i]=arr[i];
		char nam[80];
		char tit[80];
		sprintf(nam, "%s_%.1fto%.1f",name.c_str(),arr[i-1],arr[i]);
		sprintf(tit, ";%s in %.1f<=Z Pt<%.1f",name.c_str(),arr[i-1],arr[i]);
		cout<<tit<<endl;
		t.Arr.push_back(new  TH1F(nam,tit,bins,up,down));
	}
}
void DrawHA(HistoArray& a){
	GobalCanvas = new TCanvas("GobalCanvas","",1400,600);
	GobalCanvas->Divide(a.len/2+a.len%2,2);
	NowCd=0;
	CdAmount=a.len;
	for(int i=0;i<a.len;i++){
		drawDataMC(a.Arr[i]);
	}
}
void FillHA(HistoArray& a,float cod,float in,float weight){
	if(cod<a.boundary[0]) return;
	for(int i=0;i<a.len;i++){
		if(cod<a.boundary[i+1]){ 
		a.Arr[i]->Fill(in,weight);
		break;
		}
	}
}

Long64_t FillAllHA(TreeReader& dataD,int typeInt,HistoArray& theta,HistoArray& Ctheta){
//	TreeReader dataD(name);
	Long64_t ev = dataD.GetEntriesFast();
	cout<<ev<<endl;
	TotalN=0;
	for(Long64_t i=0;i<ev;i++){
		if(i%10000==0) cout<<(float)i/(float)ev*100<<"% "<<i<<" in "<<ev<<"\r";
		
		dataD.GetEntry(i);
		readggtree(dataD);
		if(fabs(lept0_eta)>2.5) continue;
		if(fabs(lept0_eta<1.566&&lept0_eta>1.4442)) continue;
		if(fabs(lept1_eta)>2.5) continue;
		if(fabs(lept1_eta<1.566&&lept1_eta>1.4442)) continue;
		if(NotPass(typeInt)) continue;
		if(fabs(z_y)>1.0) continue;
		FillHA(theta,z_pt,lep1_theta_Zrest,weight);
		FillHA(Ctheta,z_pt,lep1_costheta_Zrest,weight);
		
	}
	return TotalN;	
}
Long64_t FillOther(TreeReader& dataD,int typeInt,TH1F* PT1,TH1F* PT2,TH1F* Eta1,TH1F* Eta2,TH1F* ZMass,TH1F* ZPt,TH1F* ZY){
//	TreeReader dataD(name);
	Long64_t ev = dataD.GetEntriesFast();
	cout<<ev<<endl;
	TotalN=0;
	for(Long64_t i=0;i<ev;i++){
		if(i%5000==0) cout<<(float)i/(float)ev*100<<"% "<<i<<" in "<<ev<<endl;
		
		dataD.GetEntry(i);
		readggtree(dataD);

		if(NotPass(typeInt)) continue;

		if(lept0_pt>lept1_pt){
			PT1->Fill(lept0_pt,weight);
			PT2->Fill(lept1_pt,weight);
			Eta1->Fill(lept0_eta,weight);
			Eta2->Fill(lept1_eta,weight);
		}else{
			PT1->Fill(lept1_pt,weight);
			PT2->Fill(lept0_pt,weight);
			Eta1->Fill(lept1_eta,weight);
			Eta2->Fill(lept0_eta,weight);
		}
		ZMass->Fill(z_mass,weight);
		ZPt->Fill(z_pt,weight);
		ZY->Fill(z_y,weight);
		
	}
	return TotalN;
	
}

Long64_t FillTheta(TreeReader& dataD,int typeInt,TH1F* Theta1,TH1F* Theta2,TH1F* CTheta1,TH1F* CTheta2){
	TotalN=0;

//	TreeReader dataD(name);
	Long64_t ev = dataD.GetEntriesFast();
	cout<<ev<<endl;
	for(Long64_t i=0;i<ev;i++){
		if(i%5000==0) cout<<(float)i/(float)ev*100<<"% "<<i<<" in "<<ev<<endl;
		dataD.GetEntry(i);
		readggtree(dataD);

		if(NotPass(typeInt)) continue;

		if(fabs(z_y)<=1.){
			if(z_pt<3000.){
				if(z_pt<1000.){
					CTheta1->Fill(lep0_costheta_Zrest,weight);
					Theta1->Fill(lep0_theta_Zrest,weight);
				}
				else{
					Theta1->Fill(lep0_theta_Zrest,weight);
				}
			}
		}else if(fabs(z_y)<=2.5){
			if(z_pt<3000.){
				if(z_pt<1000.){
					CTheta2->Fill(lep0_costheta_Zrest,weight);
					Theta2->Fill(lep0_theta_Zrest,weight);
				}
				else{
					Theta2->Fill(lep0_theta_Zrest,weight);
				}
			}
		}	
		
	}
	return TotalN;
	
}

void drawTheta(TString dataName,TString MCName,int typeInt){
	TreeReader data(dataName,"OT");
	TreeReader MCdataE(MCName,"OT");
	TString type;
	if(typeInt==11){
		type="Ele ";
	}else if(typeInt==22){
		type="Mu ";
	}
	TH1F*  Theta1 	= new  TH1F("Theta1",type+"Theta;(0.0<z_pt<3000.0  0<z_y<1) ",100,0.,3.14);
	TH1F*  Theta2 	= new  TH1F("Theta2",type+"Theta;(0.0<z_pt<3000.0  1<z_y<2.5) ",100,0.,3.14);
	TH1F*  CTheta1 = new  TH1F("CTheta1",type+"CosTheta;(0.0<z_pt<1000.0  0<z_y<1) ",100,-1,1);
	TH1F*  CTheta2 = new  TH1F("CTheta2",type+"CosTheta;(0.0<z_pt<1000.0  1<z_y<2.5) ",100,-1,1.);
	TH1F*  ZMass	= new  TH1F("ZMass",type+"Z Mass",200,50.,120.);
	

	FillTheta(data,typeInt,Theta1,Theta2,CTheta1,CTheta2);
	

	GobalCanvas = new TCanvas("GobalCanvas","",600,1400);

	GobalCanvas->Divide(2,2);
	CdAmount=4;
	cout<<1;
	drawDataMC(Theta1		);
	drawDataMC(Theta2		);
	drawDataMC(CTheta1   );
	drawDataMC(CTheta2   );
	cout<<2;
	GobalCanvas->Print("Theta.pdf");

}
void drawOther(TString dataName,TString MCName,int typeInt){
	TreeReader data(dataName,"OT");
	TreeReader MCdataE(MCName,"OT");
	TString type;
	if(typeInt==11){
		type="Ele ";
	}else if(typeInt==22){
		type="Mu ";
	}
	TH1F*  Pt1 		= new  TH1F("Pt1",type+"Pt(Leading)",100,0.,120.);
	TH1F*  Pt2 		= new  TH1F("Pt2",type+"Pt",100,0.,120.);	
	TH1F*  Eta1 	= new  TH1F("Eta1",type+"Eta(Leading)",100,-2.5,2.5);
	TH1F*  Eta2 	= new  TH1F("Eta2",type+"Eta",100,-2.5,2.5);
	TH1F*  ZMass	= new  TH1F("ZMass",type+"Z Mass",200,50.,120.);
	TH1F*  ZPt		= new  TH1F("ZPt",type+"Z Pt",200,0.,200.);
	TH1F*  Z_Y		= new  TH1F("Z_Y",type+"Z Y",200,-2.6,2.6);
	
	TH1F*  McPt1 	= new  TH1F("McPt1",type+"Pt(Leading)",100,0.,120.);
	TH1F*  McPt2 	= new  TH1F("McPt2",type+"Pt",100,0.,120.);	
	TH1F*  McEta1 	= new  TH1F("McEta1",type+"Eta(Leading)",100,-2.5,2.5);
	TH1F*  McEta2 	= new  TH1F("McEta2",type+"Eta",100,-2.5,2.5);
	TH1F*  McZMass	= new  TH1F("McZMass",type+"Z Mass",200,50.,120.);
	TH1F*  McZPt	= new  TH1F("McZPt",type+"Z Pt",200,0.,200.);
	TH1F*  McZ_Y	= new  TH1F("McZ_Y",type+"Z Y",200,-2.6,2.6);

	FillOther(data,typeInt,Pt1,Pt2,Eta1,Eta2,ZMass,ZPt,Z_Y);
	

	GobalCanvas = new TCanvas("GobalCanvas","",1800,800);
	GobalCanvas->Divide(4,2);
	CdAmount=8;

	cout<<1;
	drawDataMC(Pt1 	);
	drawDataMC(Pt2 	);
	drawDataMC(Eta1 	);
	drawDataMC(Eta2 	);
	drawDataMC(ZMass	);
	drawDataMC(ZPt    );
	drawDataMC(Z_Y		);
	cout<<2;
	GobalCanvas->Print("c.pdf");
}

void drawAllTheta(TString dataName,TString MCName,int typeInt){
	TreeReader data(dataName,"OT");
//	TreeReader MCdataE(MCName,"OT");
	string type;
	if(typeInt==11){
		type="Ele ";
	}else if(typeInt==22){
		type="Mu ";
	}
	  float bou[]={0.,10.,20.,35.,55.,80.,120.,200.,400.};
	  HistoArray theta,Ctheta;
	  InitHistoArray(theta,type+" Theta",8,bou,100,0.,3.15);
	  InitHistoArray(Ctheta,type+" CosTheta",8,bou,100,-1.,1.);
	  FillAllHA(data,typeInt,theta,Ctheta);
	  
	  DrawHA(theta);
	  GobalCanvas->Print("c1.pdf");
	  int an;
	  cin>>an;
	  DrawHA(Ctheta);
	  GobalCanvas->Print("c2.pdf");
		//a.Arr[2]->Draw();
}
